<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <title><?=$site_title."|".$site_tagline?></title>
    <link rel="icon" type="image/png" href="favicon.png" />
    <link rel="stylesheet" type="text/css" href="themes/<?=$site_theme?>/styles.css" />
</head>
<body>

    <div id="all">

        <div id="header-wrapper">

            <div id="header">

                <h1><a href="<?=$site_url?>"><?=$site_title?></a></h1>
                <p><?=$site_tagline?></p>

            </div><!-- #header -->

        </div><!-- #header-wrapper -->

        <?php include "inc/main-menu.php"; ?>
