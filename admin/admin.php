<?php

/*  --- Funções ----------------------------------------------------------- */

/*
Retorna o estado de login.
*/
function login_state() {
    return (isset($_SESSION['verified']) && $_SESSION['verified'] === true);
}

/*
Captura valor da query string de ações de login.
*/
function login_action() {
    if (isset($_GET['login'])) {
        switch ($_GET['login']) {
            case 'verify';
                login_verify();
                break;
            case 'exit';
                login_exit();
                break;
        }
    }
}

/*
Verifica credenciais do usuário admin.
*/
function login_verify() {
    // Chaves para facilitar a visualização dos dados...
    $keys = ['uname', 'umail', 'lname', 'upass'];

    // Capturar dados numa array com as chaves acima...
    $data = array_combine($keys, file('data/default.data', FILE_IGNORE_NEW_LINES));

    // Avaliando as expressões de forma visual...
    $vname = ($_POST['uname'] == $data['lname']);
    $vmail = ($_POST['uname'] == $data['umail']);
    $vpass = password_verify($_POST['upass'], $data['upass']);

    // Testando as avaliações...
    if (($vname || $vmail) && $vpass) {

        // Se verdadeiro, define o estado de $_SESSION['verified'] para true...
        $_SESSION['verified'] = true;

        // Passando os dados do admin para a sessão...
        $_SESSION['umail'] = $data['umail'];
        $_SESSION['uname'] = $data['uname'];
        $_SESSION['lname'] = $data['lname'];

        // Redireciona para a página inicial....
        header('location:/admin');
    } else {
        // Se falso, redireciona para a página inicial com '?login=erro'...
        header('location:/admin/?login=error');
    }
}
/*
Encerra a sessão.
*/
function login_exit() {
    session_unset();
    session_destroy();
    header("location:/admin");
}


/* --- Principal ---------------------------------------------------------- */

/*
Inicia a sessão.
*/
session_start();

/*
Executa a função de captura das ações de login (se houver).
*/
login_action();

/*
Quando estamos no servidor do PHP, é importante definir
a URL base de todos os links para que ele não se perca
com os caminhos relativos.
*/
$base_url  = 'http://localhost:8000/admin/';

/*
Verifica se o usuário já está logado para decidir que
página mostrar.
*/
$main_page = (login_state()) ? "panel" : "login";

