<!--
:author=Blau Araujo
:tags=teste,php,cms,blog
:categories=teste,oficina
-->

# Postagem de exemplo 3

Resumo da postagem 3, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor.

<!-- more -->

* Item 1
* Item 2
* Item 3

**Lorem ipsum dolor sit amet**, consectetur adipisicing
elit, sed doeiusmod tempor incididunt ut labore et
dolore magna aliqua. *Ut enimad minim veniam*, quis
nostrud exercitation ullamco laboris nisi utaliquip ex
ea commodo consequat. ***Duis aute irure dolor***.

| Coluna 1 | Coluna 2 | Coluna 3 |
|----------|----------|----------|
| valor 01 | valor 02 | valor 03 |
| valor 11 | valor 12 | valor 13 |

Lorem ipsum dolor sit amet, consectetur adipisicing
elit, sed doeiusmod tempor incididunt ut labore et
dolore magna aliqua. Ut enimad minim veniam, quis
nostrud exercitation ullamco laboris nisi utaliquip ex
ea commodo consequat. Isso é um `código inline`.
Duis aute irure dolor.

```
# Isso é um código...
:~$ echo "Olá, debxPress!"
<html>

```

Lorem ipsum dolor sit amet, consectetur adipisicing
elit, sed doeiusmod tempor incididunt ut labore et
dolore magna aliqua. Ut enimad minim veniam, quis
nostrud exercitation ullamco laboris nisi utaliquip ex
ea commodo consequat. Duis aute irure dolor.
