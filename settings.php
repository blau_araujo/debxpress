<?php

// Configurações do site:

$site_url="http://localhost:8000";
$site_title="debxPress";
$site_tagline="Oficina CMS do zero só com PHP, HTML e CSS!";
$site_theme="default";

// Menu principal:

$main_menu=[
    'home'      => 'Início',
    'sample'    => 'Página de exemplo',
    'community' => 'Comunidade',
    'about'     => 'Sobre nós',
];
