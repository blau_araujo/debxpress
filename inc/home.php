<?php
include "settings.php";
include "cms/functions.php";
include "lib/parsedown.php";
include "inc/header.php";
?>

<div id="contents-wrapper">

    <div id="contents">

        <div id="main">

            <?php show_content(); ?>

        </div><!-- #main -->

        <?php include "inc/sidebar.php"; ?>

        <div class="cleaner"></div>

    </div><!-- #contents -->

</div><!-- #contents-wrapper -->

<?php include "inc/footer.php"; ?>
