<?php include "admin.php"; ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <title>debxPress | Painel Administrativo</title>
    <base href="<?php echo $base_url; ?>">
    <link href="favicon.png" rel="icon" type="image/png" />
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body class="<?php echo $main_page; ?>">

<?php include "inc/$main_page.php"; ?>

</body>
</html>
