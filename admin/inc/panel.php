<header>
    <div class="logo">
        <h1>debxPress</h1>
        <p>Painel Administrativo &bull; <a href="../" target="_blank">Abrir o site em outra aba</a></p>
    </div>
</header>

<main>

    <nav>
        <ul>
            <li class="admin-user"><a href="?s=profile&usr=<?php echo $_SESSION['lname']?>"><?php echo $_SESSION['uname']; ?></a></li>
            <li class="sep"></li>
            <li><a href="">Acesso Rápido</a></li>
            <li><a href="?s=posts">Artigos</a></li>
            <li><a href="?s=pages">Páginas</a></li>
            <li><a href="?s=settings">Configurações do Site</a></li>
            <li><a href="?s=styles">Temas e Estilos</a></li>
            <li><a href="?s=menus">Gerenciar Menus</a></li>
            <li><a href="?s=users">Utilizadores</a></li>
            <li class="sep"></li>
            <li><a href="?login=exit">Sair</a></li>
        </ul>
    </nav>

    <section>

<?php
include 'inc/'.((isset($_GET['s'])) ? $_GET['s'] : 'dashboard').'.php';
?>

<pre>

<?php
echo 'Sessão salva em: '.session_save_path().PHP_EOL;
echo 'Arquivo da sessão: sess_'.session_id().PHP_EOL;
?>

</pre>

    </section>

    <div class="clear"></div>

</main>

<footer>
    <div class="footer">
        <p>Powered by <a href="https://gitlab.com/blau_araujo/debxpress">debxPress</a> - Copyleft <span class="copyleft">&copy;</span> 2020-2021, <a href="">debxp/cursos</a></p>
        <p>Este é um Software Livre distribuído sob os termos da licença <a href="../LICENSE">GNU/GPL v3.0</a></p>
    </div>
</footer>
